package controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import service.DataSekolahService;

public class DataSekolahController {
    private static final String line = "=======================================================================================";
    private static final String newLine = "\n";
    private static final DataSekolahService dts = new DataSekolahService();

    /***
     * Write result of grouped data from DataSekolah to
     * grouping_result.txt with BufferedWriter.
     * 
     * @param txtFile directory of grouping_result.txt
     *                will be saved.
     * @param input   parameter for number
     *                as standard score.
     */
    public void writeGrouping(String txtFile, int input) {
        try {
            File file = new File(txtFile);
            String inputString = Integer.toString(input);
            if (file.createNewFile()) {
                System.out.println("New file is created!");
            }
            FileWriter wtr = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(wtr);
            bwr.write("Hasil pengolahan nilai: \n");
            bwr.write(line);
            bwr.write(newLine);
            bwr.write("Pengelompokkan nilai dengan acuan nilai ");
            bwr.write(inputString);
            bwr.write(" beserta jumlah siswa tiap nilai \n");
            bwr.write(line);
            bwr.write(newLine);
            bwr.write("Daftar nilai yang kurang dari ");
            bwr.write(inputString);
            bwr.write(newLine);
            bwr.write(dts.writeLessThan(input));
            bwr.write(newLine);
            bwr.write(newLine);
            bwr.write("Jumlah siswa yang bernilai ");
            bwr.write(inputString);
            bwr.write(" adalah ");
            bwr.write(dts.getValueOf(input));
            bwr.write(newLine);
            bwr.write(newLine);
            bwr.write("Daftar nilai yang lebih dari ");
            bwr.write(inputString);
            bwr.write(newLine);
            bwr.write(dts.writeMoreThan(input));
            bwr.write(newLine);
            bwr.write(newLine);
            bwr.flush();
            bwr.close();
            System.out.println("Succsessfully write to new file!");
        } catch (IOException e) {
            System.out.println("An error occured!");
            e.printStackTrace();
        }
    }

    /***
     * Write result of basic statistic operation of
     * data from DataSekolah to basic_statistic_result.txt with BufferedWriter.
     * 
     * @param txtFile directory of grouping_result.txt
     *                will be saved.
     */
    public void writeBasicStatistic(String txtFile) {
        try {
            File file = new File(txtFile);
            if (file.createNewFile()) {
                System.out.println("New file is created!");
            }
            FileWriter wtr = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(wtr);
            bwr.write("Hasil pengolahan nilai: \n");
            bwr.write(line);
            bwr.write(newLine);
            bwr.write("Nilai rata-rata = ");
            bwr.write(Double.toString(dts.calcMean()));
            bwr.write(newLine);
            bwr.write(newLine);
            bwr.write("Nilai median = ");
            bwr.write(Double.toString(dts.calcMedian()));
            bwr.write(newLine);
            bwr.write(newLine);
            bwr.write("Nilai modus = ");
            bwr.write(Integer.toString(dts.calcMode()));
            bwr.write(newLine);
            bwr.write(newLine);
            bwr.flush();
            bwr.close();
            System.out.println("Succsessfully write to new file!");
        } catch (IOException e) {
            System.out.println("An error occured!");
            e.printStackTrace();
        }
    }
}
